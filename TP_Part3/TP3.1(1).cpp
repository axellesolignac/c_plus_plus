#include <iostream>
#include "Carte.h"

using namespace std;

int Play(){

    Carte c1(Couleur::PIQUE, "As");
    c1.afficher();
    Carte c2 (c1);
    c2.afficher();
    c2.setType(Couleur::TREFLE);
    c2.setValeur("Queen");
    c2.afficher();
    Carte c3(Couleur::PIQUE, "2");
    c2.affecter(c3);
    c2.afficher();
    c3.afficher();

    if (c1.equal(c2)){
        cout<<"is ok"<<endl;
    }
    else {
        cout<<"problem bug"<<endl;
        c1.afficher();
        c2.afficher();
    }
    return 0;
}

void Carte::setType(Couleur color) {
   this->couleur=color;
}

string Carte::setValeur(std::string value) {
    return this->valeur=value;
}

void Carte::afficher() {
    cout << Carte::TypeCarte[unsigned(this->couleur)] <<" & "<<this->valeur<<endl;
//    static_cast<int>(this->couleur)
}

void Carte::affecter(Carte &carte) {
    this->valeur=carte.valeur;
    this->couleur=carte.couleur;
}

bool Carte::equal(Carte &carte) {
    if (&carte == this)return true;
    else return false ;
}

Carte::Carte(const Carte &carte) :couleur(carte.couleur), valeur(carte.valeur){}

Carte::~Carte() {}