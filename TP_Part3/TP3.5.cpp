#include <iostream>

using namespace std;

class ICarte_YUGIOH {
public :
    virtual void afficher() = 0;
    virtual string getDescription() = 0;
    virtual string getNomClasse() = 0;
    virtual string getNumeroCarte() = 0;

    virtual ~ICarte_YUGIOH() = default;
};
