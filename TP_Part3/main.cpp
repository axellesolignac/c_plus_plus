#include <iostream>
#include "Fonctions.h"
#include "Dumbproof.h"
using namespace std;
int getMenuOption();

int main() {
    int choice;
    while (true) {
        choice = getMenuOption();
        if (choice == 6) { break; }
    }
}
int getMenuOption(){
    int urchoice(0);
    cout << "********************* Menu Part 1 TP C++ *********************" << endl << endl;
    cout << " 1- Jeu de carte V1" << endl;
    cout << " 2- Jeu de carte V2" << endl;
    cout << " 3- YOGIOH : Cartes monstres" << endl;
    cout << " 4- YOGIOH : Cartes magies & pi\x8Ages" << endl;
    cout << " 5- YOGIOH : Version g\x82n\x82rale" << endl;
    cout << " 7- Quitter" << endl << endl;
    cout << "Vous choisissez quelle option ?" << endl;
    cinSecure(urchoice);

    cout << "Vous avez entre : " << urchoice << endl;

    switch (urchoice) {
        case 1:
            cout << "********************* Bienvenu dans Jeu de carte V1 *********************" << endl;
            Play();
            return 1;
        case 2:
            cout << "********************* Bienvenu dans Jeu de carte V2 *********************" << endl;
            Play2();
            return 2;
        case 3:
            cout << "********************* Bienvenu dans YOGIOH : Cartes monstres *********************" << endl;
            Test_CarteMonstre();
            return 3;
        case 4:
            cout << "********************* Bienvenu dans YOGIOH : Cartes magies & pi\x8Ages *********************" << endl;
            Test_CarteMagiePiege();
            return 4;
        case 5:
            cout << "********************* Bienvenu dans YOGIOH : Version g\x82n\x82rale *********************" << endl;
            return 5;

        case 7:
            cout << "A Bientot" << endl;
            exit(EXIT_SUCCESS);

        default:
            cout << "La saisie n'est pas une option valide." << endl;
    }
}