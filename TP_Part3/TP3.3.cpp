#include <iostream>
#include <utility>

using namespace std;

enum Attribut {
    TENEBRE, TERRE, FEU, LUMIERE, EAU, VENT
};

class Carte_Monstre {
private :
    int a_ATK;
    int a_DEF;
    string a_description;
    int a_niveau;
    string a_nomcarte;
    string a_numerocarte;
    string a_type;
    Attribut a_attribut;

public :
    Carte_Monstre(int ATK, int DEF, string description, int niveau, string nomcarte, string numerocarte, string type,
                  Attribut attribut) :
            a_ATK(ATK),
            a_DEF(DEF),
            a_description(std::move(description)),
            a_niveau(niveau),
            a_nomcarte(std::move(nomcarte)),
            a_numerocarte(std::move(numerocarte)),
            a_type(std::move(type)),
            a_attribut(attribut) {
    }

    void afficher() {
        cout << "ATK :" << this->getATK() << "; DEF :" << this->getDEF() << "; DESC :" <<
             this->getDescription() << "; LVL :" << this->getNiveau() << "; NAME :" << this->getNomCarte() <<
             "; NUMBER :" << this->getNumeroCarte() << "; TYPE :" << this->getType() << "; ASPECT :"
             << this->getAttribut() << endl;
    }

    int getATK() const { return this->a_ATK; }

    int getDEF() const { return this->a_DEF; }

    int getNiveau() const { return this->a_niveau; }

    Attribut getAttribut() { return this->a_attribut; }

    string getDescription() { return this->a_description; }

    string getNomCarte() { return this->a_nomcarte; }

    string getNumeroCarte() { return this->a_numerocarte; }

    string getType() { return this->a_type; }

    ~Carte_Monstre() = default;
};

void Test_CarteMonstre() {
    cout << "Test des fonctions du TP 3.3 :" << endl;
    cout
            << "La carte monstre suivante est créé :\nATK:69\nDEF:420\ndescription : Ceci est une carte\nniveau : 5\nnomcarte : carteTest\nnumerocarte : 69420\ntype : Mage\nAttribut : EAU\n";
    cout << "La fonction afficher() appelle toute les autres, je vous laisse regarder dans le code\n";

    Carte_Monstre test = Carte_Monstre(69, 420, "Ceci est une carte", 5, "carteTest", "69420", "Mage", Attribut::EAU);
    test.afficher();
}