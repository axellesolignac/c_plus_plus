#ifndef TP_PART3_CARTE_H
#define TP_PART3_CARTE_H

#include <array>
enum class Couleur:unsigned short{PIQUE=0,COEUR=1,CARREAU=2,TREFLE=3};
class Carte {
    public:
        std::array<std::string,4> TypeCarte = {"Pique","Coeur","Carreau","Trefle"};
        Couleur couleur;
        std::string valeur;
        Carte(Couleur c, std::string v):couleur(c),valeur(v){}
        Carte(const Carte& carte);
        virtual ~Carte();
        void setType(Couleur color);
        std::string setValeur(std::string value);
        void afficher();
        bool equal(Carte &carte);
        void affecter(Carte& carte);

};
#endif //TP_PART3_CARTE_H
