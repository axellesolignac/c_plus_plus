#include <iostream>

using namespace std;

enum Icone {
    Equipement, Terrain, JeuRapide, Contre, Continue, Rituel
};

class Carte_MagiePiege {
private:
    string a_descriptioncarte;
    string a_nomcarte;
    string a_numerocarte;
    Icone a_icone;

public:
    Carte_MagiePiege(string descriptioncarte, string nomcarte, string numerocarte, Icone icone) :
            a_descriptioncarte(std::move(descriptioncarte)),
            a_nomcarte(std::move(nomcarte)),
            a_numerocarte(std::move(numerocarte)),
            a_icone(icone) {
    }

    virtual void afficher() {
        cout << "DESC :" << this->getDescription() << "; NAME :" << this->getNomCarte() << "; NUMBER :" <<
             this->getNumeroCarte() << "; ICON :" << this->getIcone();
    }

    virtual string getDescription() { return this->a_descriptioncarte; }

    virtual Icone getIcone() { return this->a_icone; }

    virtual string getNomCarte() { return this->a_nomcarte; }

    virtual string getNumeroCarte() { return this->a_numerocarte; }

    virtual ~Carte_MagiePiege() = default;

};

class Carte_Magie : public Carte_MagiePiege {
public:
    void afficher() override {
        cout << "DESC :" << this->getDescription() << "; NAME :" << this->getNomCarte() << "; NUMBER :" <<
             this->getNumeroCarte() << "; ICON :" << this->getIcone() << endl;
    }

    Carte_Magie(string descriptioncarte, string nomcarte, string numerocarte, Icone icone) : Carte_MagiePiege(
            std::move(descriptioncarte),
            std::move(nomcarte),
            std::move(numerocarte),
            icone) {};

    ~Carte_Magie() override = default;
};

class Carte_Piege : public Carte_MagiePiege {
public:
    void afficher() override {
        cout << "DESC :" << this->getDescription() << "; NAME :" << this->getNomCarte() << "; NUMBER :" <<
             this->getNumeroCarte() << "; ICON :" << this->getIcone() << endl;
    }

    Carte_Piege(string descriptioncarte, string nomcarte, string numerocarte, Icone icone) : Carte_MagiePiege(
            std::move(descriptioncarte),
            std::move(nomcarte),
            std::move(numerocarte),
            icone) {};

    ~Carte_Piege() override = default;
};

void Test_CarteMagiePiege() {
    cout << "Test des fonctions du TP 3.4 :\n";
    cout << "Creation et affichage de CarteMagie\n";

    Carte_Magie testCarteMagie = Carte_Magie("Description", "Nom de la carte", "Numéro de la carte", Icone::Continue);
    testCarteMagie.afficher();

    cout << "Creation et affichage de CartePiege\n";

    Carte_Piege testCartePiege = Carte_Piege("Description", "Nom de la carte", "Numéro de la carte", Icone::Continue);
    testCartePiege.afficher();
}