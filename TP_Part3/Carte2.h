//
// Created by axous on 15/03/2021.
//

#ifndef TP_PART3_CARTE2_H
#define TP_PART3_CARTE2_H
enum class Couleur2:unsigned short{PIQUE=0,COEUR=1,CARREAU=2,TREFLE=3};
class Carte2 {
public:
    Couleur2 couleur;
    std::string valeur;
    Carte2(Couleur2 c, std::string v):couleur(c),valeur(v){}
    Carte2(const Carte2& carte2);
    virtual ~Carte2();
    Carte2 &operator=(Carte2 &carte2);
    bool operator==(Carte2 &carte2);
    bool operator!=(Carte2 &carte2);
    void setType2(Couleur2 color2);
    std::string setValeur2(std::string value);
    friend std::ostream &operator<<(std::ostream &ostream1, Carte2 &carte2);
};

std::ostream &operator<<(ostream &ostream1, Carte2 &carte2) {
    std::array<std::string,4> TypeCarte2 = {"Pique","Coeur","Carreau","Trefle"};
    ostream1 << TypeCarte2[unsigned(carte2.couleur)] << " & " << carte2.valeur << endl;
    return ostream1;
}

bool Carte2::operator!=(Carte2 &carte2) {
    if (&carte2 != this)   return true;
    return false;
}
bool Carte2::operator==(Carte2 &carte2) {
    if (&carte2 == this)   return true;
    return false;
}
Carte2 &Carte2::operator=(Carte2 &carte2) {
    if (&carte2 != this){
        this->valeur = carte2.valeur;
        this->couleur = carte2.couleur;
    }
    return *this;
}
std::string Carte2::setValeur2(std::string value) {
    return this->valeur=value;
}

void Carte2::setType2(Couleur2 color2) {
    this->couleur=color2;
}

Carte2::Carte2(const Carte2 &carte2) :couleur(carte2.couleur), valeur(carte2.valeur){}

Carte2::~Carte2() {}

#endif //TP_PART3_CARTE2_H
