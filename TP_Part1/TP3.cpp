#include <iostream>
#include <conio.h>
#include "Dumbproof.h"

using namespace std;

void Allumette() {
    bool joueur = true;
    int nbAllumette, nbEnleve;
    cout << "Choisir le nombre d'allumette de depart :", cinSecure(nbAllumette); //au lieu de cin >> nbAllumette
    while (nbAllumette > 0) {
        for (int i = 0; i < nbAllumette; i++) { cout << "| "; }
        cout << (joueur ? "joueur 1 enleve :" : "joueur 2 enleve :"), cinSecure(nbEnleve);
        nbAllumette = nbAllumette - nbEnleve;
        joueur = !joueur;
    }
    cout << (joueur ? "le joueur 2 a perdu :-(" : "le joueur 1 a perdu :-(") << endl;
    getch();
}