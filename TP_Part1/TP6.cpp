#include <iostream>
#include <conio.h>
#include "Dumbproof.h"

using namespace std;

int Fibonacci() {
    int n, nbr1 = 0, nbr2 = 1, suivant;
    cout << "Entrez le nombre de termes" << endl;
    cinSecure(n);
    cout << "Les %d premiers termes de la série de Fibonacci sont:" << n << endl;

    for (int i = 0; i < n; i++) {
        if (i <= 1)
            suivant = i;
        else {
            suivant = nbr1 + nbr2;
            nbr1 = nbr2;
            nbr2 = suivant;
        }
        cout << suivant << endl;
    }
    getch();
    return 0;
}