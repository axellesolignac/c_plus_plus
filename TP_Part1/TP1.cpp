#include <iostream>
#include <ctime>
#include <conio.h>
#include "Dumbproof.h"
#include "TP_main.h"


using namespace std;

void Nombre_Mystere() {
    srand(time(0));
    int mystere = rand() % 20 - 10;
    int nessais = 0;
    int nb = -1;
    while (nb != mystere) {
        cout << "Entrez un entier : ";
        cinSecure(nb); //au lieu de cin >> nb
        ++nessais;
        if (nb == mystere) { cout << "==> Gagn\x82" << endl; }
        else if (nb < mystere) { cout << "==> Trop petit" << endl; }
        else { cout << "==> Trop grand" << endl; }
    }
    cout << "Trouve en " << nessais << " essai(s)" << endl;
    getch();
}