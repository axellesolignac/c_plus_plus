#include <iostream>
#include <conio.h>
#include <math.h>
#include "TP8.cpp"

double power(double x, int y) {
    double temp;
    if (y == 0)
        return 1;
    temp = power(x, y / 2);
    if (y % 2 == 0)
        return temp * temp;
    else {
        if (y > 0)
            return x * temp * temp;
        else
            return (temp * temp) / x;
    }
}

void cosinus() {
    double x, resultat = 0;
    std::cout << "Entrez un chiffre :";
    cinSecure(x);

    for (int i = 0; i < 100; ++i) {
        resultat += (power(-1, i) / fact((2 * i))) * power(x, (2 * i));

    }

    std::cout << "ma reponse : cos(" << x << ")=" << resultat << std::endl;
    std::cout << "reponse de math.h : cos(" << x << ")=" << cos(x) << std::endl;
    getch();
}

void sinus() {
    double x, resultat = 0;
    std::cout << "Entrez un chiffre :";
    cinSecure(x);

    for (int i = 0; i < 100; ++i) {
        resultat += power(-1, i) * (power(x, (2 * i + 1)) / fact(2 * i + 1));
    }

    std::cout << "ma reponse : sin(" << x << ")=" << resultat << std::endl;
    std::cout << "reponse de math.h : sin(" << x << ")=" << sin(x) << std::endl;
    getch();

}

int DevLimites() {
    int userChoice;
    std::cout << "Calculer un cosinus (1) ou un sinus (2) ?";
    cinSecure(userChoice);

    if (userChoice == 1) { cosinus(); }
    else if (userChoice == 2) { sinus(); }
    else {
        std::cout << "cette option n'est pas valide" << std::endl;
        DevLimites();
    }

    return 0;
}