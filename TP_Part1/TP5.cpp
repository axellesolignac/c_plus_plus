#include <iostream>
#include <cmath>
#include <conio.h>
#include "Dumbproof.h"

using namespace std;


void Integrale() {
    double a, b, p, inf, sup;
    cout << "Debut de l'interval :" << endl, cinSecure(a); //au lieu de cin >> a;
    cout << "Fin de l'interval :" << endl, cinSecure(b); //au lieu de cin >> b;
    cout << "Pas :" << endl, cinSecure(p); //au lieu de cin >> p;
    double n = (b - a) / p;
    for (int i = 0; i < n; ++i) {
        double l = (b - a) / n;
        double hinf = pow((a + (i * (b - a) / n)), 2);
        double hsup = pow((a + ((i + 1) * (b - a) / n)), 2);
        inf += l * hinf;
        sup += l * hsup;
    }
    double moy = (inf + sup) / 2;
    cout << "D'apres la methode des rectangles, l'integralle de x^2 sur[" << a << ";" << b << "] avec un pas de " << p
         << " est :" << moy << endl;
    getch();
}