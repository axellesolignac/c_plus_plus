#include <iostream>
#include <conio.h>
#include "Dumbproof.h"

using namespace std;

double fact(double n) {
    double facto = 1;
    for (int i = 1; i <= n; ++i)
        facto *= i;
    return facto;
}

double Maxfact() {
    double n;
    cout << "Calcul du factoriel" << endl;
    cout << "Entrer un entier : " << endl;
    cinSecure(n);
    cout << n << "!=" << fact(n) << endl;
    getch();
    return 0;
}