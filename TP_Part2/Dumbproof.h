//
// Created by axous on 16/02/2021.
//

#ifndef TP_PART2_DUMBPROOF_H
#define TP_PART2_DUMBPROOF_H
#include <iostream>
#include <limits>
using namespace std;

template<typename T> void cinSecure(T &var)
{
    while (!(cin >> var))
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << "Saisie incorrecte, merci de retaper." << endl;
    }
}
#endif //TP_PART2_DUMBPROOF_H
