#define DEBUG 1

#if (DEBUG == 1)
#define TEST_INPUT(var) \
    while(!(cin >> var)){       \
        cin.clear();            \
        cin.ignore(numeric_limits<streamsize>::max(),'\n'); \
        cout << "Erreur : Saisie incorrecte " << endl << "    Dans : " << __FILE__ << endl <<"    Ligne : " << __LINE__ << endl; \
        cout << "Re-saisir un entier :" << endl;                \
    }
#endif

#define RETURN_VALUE(x, y) \
    cout << "Les deux entiers sont " << x << ", " << y << endl; \


#include <iostream>
#include <limits>
#include <conio.h>

using namespace std;

void Macro() {
    int i, j = -1;
    cout << "Rentrer le premier entier : " << endl;
    TEST_INPUT(i)
    cout << "Rentrer le second entier : " << endl;
    TEST_INPUT(j)
    RETURN_VALUE(i, j)

    getch();
}