#include <iostream>
#include <conio.h>

#define TEST(var) \
    while(!(cin >> var)){       \
        cin.clear();            \
        cin.ignore(numeric_limits<streamsize>::max(),'\n'); \
        cout << "Erreur : Saisie incorrecte"<< endl;        \
    }

using namespace std;

enum Sexe {
    INCONNUE = 0, MASCULIN = 1, FEMININ = 2
};

struct Personne {
    int numero;
    char nom[10];
    Sexe sexe;
};

Personne *creer() {
    return new Personne;
}

Personne *detruire(Personne *ptr) {
    delete ptr;
    cout << "    Pointeur Detruit";
}

void initialiser(Personne &ptr) {
    int genre;
    cout << "    Entrez un numero :" << endl;
    TEST(ptr.numero)
    cout << "    Entrez un nom (10 char max) :" << endl;
    TEST(ptr.nom)
    cout << "    Entrez le sexe (0, 1, 2) : " << endl;
    TEST(genre)
    if (genre != 1 || genre != 2 || genre != 3) {
        cout << "Erreur : Saisie incorrecte" << endl;
        TEST(genre);
    }
    (genre == 0) ? (ptr.sexe = Sexe::INCONNUE) : (
            (genre == 1) ? (ptr.sexe = Sexe::MASCULIN) : (ptr.sexe = Sexe::FEMININ)
    );

    cout << "    Champs de la structure Personne initialises" << endl;
}

void afficher(Personne &ptr) {
    cout << "    Nom : " << ptr.nom << endl << "    Numero : " << ptr.numero << endl << "    Sexe : " << ptr.sexe
         << endl;
}

void Personne() {

    cout << "Appel de la fonction cree() :";
    getch();
    struct Personne *ptr = creer();
    cout << "   Adresse du pointeur : " << ptr << endl;
    getch();

    cout << "Appel de la fonction initialiser() :";
    getch();
    initialiser(*ptr);
    getch();

    cout << "Appel de la fonction afficher() :";
    getch();
    afficher(*ptr);
    getch();

    cout << "Appel de la fonction detruire() :";
    getch();
    detruire(ptr);
    getch();
}