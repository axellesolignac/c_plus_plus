#include <iostream>

using namespace std;
typedef double* ptrDouble;

void constructeur(ptrDouble &dd, const unsigned & size){
    dd = new double[size]{0};
}
void afficher(const ptrDouble& dd, const unsigned& size){
    if (dd != nullptr){
        for (unsigned i=0; i < size; ++i){
            cout<<"tab["<<i<<"]="<<dd[i]<<endl;
        }
    }
}
void modifier(ptrDouble& dd, const unsigned & size, const unsigned& index, const double& value){
    if (index >= size) throw (string("Error 001"));
    dd[index]=value;
}
void destructeur(ptrDouble& dd, unsigned & size){
    delete [] dd;
    dd = nullptr;
    size = 0;
}
double& get(ptrDouble& dd, const unsigned& size, const unsigned& index){
    return dd[index];
}

/*const double& get(const ptrDouble& dd, unsigned& size, const unsigned& index){
    return dd[index];
}*/

void PointeurB(){
    ptrDouble d1;
    unsigned t1 = 5;
    constructeur(d1,t1);
    afficher(d1,t1);
    modifier(d1,t1,2,3.13589985);
    afficher(d1,t1);
    cout<<get(d1,t1,2)<<endl;
    get(d1,t1,2) = 62.1;
    cout<<get(d1,t1,2)<<endl;
    afficher(d1,t1);
    destructeur(d1,t1);
    cout<<"Le pointeur d1 est libre "<<endl;
    afficher(d1,t1);//Rien ne s'affiche car notre destructeur à fait le travail
}