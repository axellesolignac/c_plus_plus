#include <iostream>
#include <memory>
using namespace std;
using ptrStdDouble = shared_ptr<double>;

void construct(ptrStdDouble &dd, const unsigned & size){
    dd.reset(new double[size]{0},default_delete<double[]>());
}
void display(const ptrStdDouble& dd, const unsigned& size){
    if (dd != nullptr){
        for (unsigned i=0; i < size; ++i){
            cout<<"tab["<<i<<"]="<<dd.get()[i]<<endl;
        }
    }
}
void edit(ptrStdDouble& dd, const unsigned & size, const unsigned& index, const double& value){
    dd.get()[index]=value;
}
void destruct(ptrStdDouble& dd, unsigned & size){
    dd.reset();
}
double& getValue(ptrStdDouble& dd, const unsigned& size, const unsigned& index){
    return dd.get()[index];
}

const double& get(const ptrStdDouble& dd,const unsigned& size, const unsigned& index){
    return dd.get()[index];
}

void PointeurC(){
    ptrStdDouble d1;
    unsigned t1 = 5;
    construct(d1, t1);
    display(d1, t1);
    edit(d1, t1, 2, 3.13589985);
    display(d1, t1);
    cout<<getValue(d1,t1,2)<<endl;
    getValue(d1,t1,2) = 62.1;
    cout<<getValue(d1,t1,2)<<endl;
    display(d1,t1);
    destruct(d1,t1);
    cout<<"Le pointeur d1 est libre "<<endl;
    display(d1,t1);
}