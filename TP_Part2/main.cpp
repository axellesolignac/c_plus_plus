#include <iostream>
#include "Fonctions.h"
#include "Dumbproof.h"
using namespace std;
int getMenuOption();

int main() {
    int choice;
    while (true) {
        choice = getMenuOption();
        if (choice == 6){break;}
    }
}
int getMenuOption() {
    int urchoice(0);
    cout << "********************* Menu Part 1 TP C++ *********************" << endl << endl;
    cout << " 1- Reference & Pointeur" << endl;
    cout << " 2- Reference & Pointeur : Corriger et completer" << endl;
    cout << " 3- Reference & Pointeur : Corriger et completer V2" << endl;
    cout << " 4- Macros" << endl;
    cout << " 5- Personne" << endl;
    cout << " 6- Quitter" << endl << endl;
    cout << "Vous choisissez quelle option ?" << endl;
    cinSecure(urchoice); //au lieu de cin >> a

    cout << "Vous avez entre : " << urchoice << endl;

    switch (urchoice) {
        case 1:
            cout << "********************* Bienvenu dans Reference et pointeur *********************" << endl;
            PointeurA();
            return 1;
        case 2:
            cout << "********************* Bienvenu dans Reference & Pointeur : Corriger et completer *********************" << endl;
            PointeurB();
            return 2;
        case 3:
            cout << "********************* Bienvenu dans Reference & Pointeur : Corriger et completer V2 *********************" << endl;
            PointeurC();
            return 3;

        case 4:
            cout << "********************* Bienvenu dans Macro *********************" << endl;
            Macro();
            return 4;

        case 5:
            cout << "********************* Bienvenu dans Personne *********************" << endl;
            Personne();
            return 5;

        case 6:
            cout << "A Bientot" << endl;
            exit(EXIT_SUCCESS);
        default:
            cout << "La saisie n'est pas une option valide." << endl;
    }
}