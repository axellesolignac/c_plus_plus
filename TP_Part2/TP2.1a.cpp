#include <iostream>
#include <memory>
using namespace std;
using ptr = int*;

void affiche(ptr p){
    cout<<"Adresse pointeur = "<<p<<endl<<"Valeur pointeur = "<<*p<<endl;
}
void constructeur(ptr p){
    int entier = 15;
    *p = entier;
}
void destructeur(ptr p){
    delete p;
    *p = 0;
}
void PointeurA(){
    int *ptr(0);
    ptr = new int;
    constructeur(ptr);
    affiche(ptr);
    destructeur(ptr);
    affiche(ptr);
}